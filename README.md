# Angular Start template

Using :
* [Angular Material -> UI component library]("https://material.angular.io/")
* [Jest -> testing library]("https://jestjs.io/")
* Translate service implemented
* Custom SVG icon implemented
* Routing

## Commands

### **Run app**
``` npm run start```

### **Run tests**
``` npm run test ```