import { Component, OnInit } from '@angular/core';
import { MatIconRegistry } from '@angular/material/icon';
import { DomSanitizer } from '@angular/platform-browser';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit{
  title = 'Project';
  public loaded = false;

  /**
   * Constructor.
   * @param matIconRegistry mat icon registry 
   * @param domSanitizer dom sanitizer -> true icon svg
   */
  constructor(private readonly matIconRegistry: MatIconRegistry, 
    private readonly domSanitizer: DomSanitizer,
    private readonly translateService: TranslateService) {}

  ngOnInit(): void {
    this.translateService.setDefaultLang('en');
    this.translateService.use('en');

    this.matIconRegistry.addSvgIcon("home", this.domSanitizer.bypassSecurityTrustResourceUrl("/src/assets/images/home.svg"));
    this.loaded = true;
  }
}
